CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers





INTRODUCTION
------------

This module enables you to setup multiple weight, size and locality based
shipping services.

Each service has his own maximum dimenstion and weight settings and a basic rate
Each service can contain one or multiple zones, to apply extra charges per 
weight, size or locality.

 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/rbosscher/1736626

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/1736626






REQUIREMENTS
------------

This module requires the following modules:

Requires: 
  *  Core 
        *  Options (enabled), 
        *  Field (enabled), 
        *  Field SQL storage (enabled), 
        *  List (enabled)
  *  Chaos tools (https://www.drupal.org/project/ctools), 
  *  Commerce (https://www.drupal.org/project/commerce), 
        *  Line Item (enabled), 
        *  Price (enabled), 
        *  Order (enabled), 
        *  Customer (enabled), 
        *  Product (enabled), 
        *  Product Reference (enabled), 
        *  Shipping (enabled), 
        *  Payment (enabled), 
  *  Entity API (https://www.drupal.org/project/entity), 
        *  Entity tokens (enabled), 
  *  Rules (https://www.drupal.org/project/rules), 
  *  Physical Fields (https://www.drupal.org/project/physical), 
  *  Physical Product (https://www.drupal.org/project/commerce_physical), 
  *  Address Field (https://www.drupal.org/project/addressfield), 




RECOMMENDED MODULES
-------------------

 * Commerce Shipping Matrix UI (Ships with this module):
   When enabled, you are enabled to configure the shipping services trough the 
   commerce_shipping ui 

 * Rules UI (Ships with the rules module)
   When enabled you are able to configure specific rules for a shipping service
   Trough the Rules UI module




INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.




CONFIGURATION
-------------
 

 * Customize the shipping service settings:
   /admin/commerce/config/shipping/shipping-matrix/settings

 * Add a new shipping service: 
   /admin/commerce/config/shipping/shipping-matrix




TROUBLESHOOTING
---------------

 * If the module gives errors on install about fields, disable and uinstall the 
   module and try enabling it again.




FAQ
---




MAINTAINERS
-----------

Current maintainers:
 * Rick Bosscher (rbosscher) - https://www.drupal.org/user/373345


Special thanks to:

* Stella - https://www.drupal.org/user/66894
