<?php


/**
 * Returns a administration page for Matrix shipping zones.
 */
function commerce_shipping_matrix_ui_zones_admin_page($shipping_service) {
  $content = array();
  $content['table'] = commerce_shipping_matrix_ui_zones_list_entities($shipping_service);

  return $content;
}

/**
 * Returns a render array with all Matrix shipping zones.
 */
function commerce_shipping_matrix_ui_zones_list_entities($shipping_service) {
  $content = array();

  // Load all shipping matrix shipping zones.
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'shipping_matrix_zone')
                    ->propertyCondition('shipping_zone_shipping_service_id', $shipping_service)
                    //->fieldCondition('field_date', 'value', array('2011-03-01', '2011-03-31'), 'BETWEEN')
                    //->fieldOrderBy('field_date', 'value', 'ASC')
                    ->execute();
  if (!empty($entities)) {
    $entities = commerce_shipping_matrix_ui_zones_load_multiple(array_keys($entities['shipping_matrix_zone']));
    foreach ($entities as $entity) {
      $ops[0] = array(
        'data' => array(
          'edit' => l(t('edit this zone'), 'admin/commerce/config/shipping/shipping-matrix/' . $shipping_service . '/zones/' . $entity->shipping_zone_id),
          'zone' => l(t('edit shipping weight table'), 'admin/commerce/config/shipping/shipping-matrix/' . $shipping_service . '/zones-table/' . $entity->shipping_zone_id),
        ),
        'no_striping' => true
      );
      $operations = array(
        '#theme' => 'table',
        '#rows' => $ops
      );

      $entity_wrapper = entity_metadata_wrapper('shipping_matrix_zone', $entity);
      $base_price = $entity_wrapper->shipping_matrix_price->value();
      //$max_dimensions = $entity_wrapper->shipping_max_shipping_dimensions->value();
      $max_weight = $entity_wrapper->shipping_matrix_weight->value();
      $rows[] = array(
        'data' => array(
          'id' => $entity->shipping_zone_id,
          'item_description' => $entity->shipping_zone_name,
          'default_rate' =>  !empty($base_price) ? commerce_currency_format($base_price['amount'], $base_price['currency_code']) : '',
       //   'max_dimensions' =>  !empty($max_dimensions) ? physical_dimensions_format($max_dimensions) : '',
          'max_weight' =>  !empty($max_weight) ? physical_weight_format($max_weight) : '',
          'operations' => drupal_render($operations),
        ),
      );
    }
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      //'#header' => array(t('ID'), t('Shipping zone'), t('Basic Shipping Rate'), t('Max Dimensions'), t('Max Weight'), t('Opperations')),
      '#header' => array(t('ID'), t('Shipping zone'), t('Basic Shipping Rate'), t('Max Weight'), t('Opperations')),
    );
  }
  else {
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No Matrix shipping zone currently exist.'),
    );
  }
  return $content;
}

function commerce_shipping_matrix_ui_zones_load_multiple($basic_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('shipping_matrix_zone', $basic_ids, $conditions, $reset);
}


/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function commerce_shipping_matrix_ui_zones_add($shipping_service) {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('shipping_matrix_zone')->create();
  return drupal_get_form('commerce_shipping_matrix_ui_zones_form', $entity, $shipping_service);
}

/**
 * Form function to create an entity_example_basic entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function commerce_shipping_matrix_ui_zones_form($form, &$form_state, $entity, $shipping_service) {
  // Load entity by id, if entity is passed as id.
  if (!is_object($entity) && ctype_digit($entity)) {
    $entities = commerce_shipping_matrix_ui_zones_load_multiple(array($entity));
    $entity = $entities[$entity];
  }

  // Confirm deletion.
  if (isset($form_state['storage']) && $form_state['storage']['ask_confirm'] ) {
    $form['basic_entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    $question = t('Are you sure you want to delete this zone?');
    $path = current_path();
    $description = t('This cannot be undone.');
    $yes = t('Confirm');
    $no = t('Cancel');
    return confirm_form($form, $question, $path, $description, $yes, $no);
  }


  // Not deleting the zone, show the standard form.
  $form['shipping_zone_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping zone name'),
    '#required' => TRUE,
    '#default_value' => !empty($entity->shipping_zone_name) ? $entity->shipping_zone_name : '',
  );
  $form['shipping_service'] = array(
    '#type' => 'hidden',
    '#value' => $shipping_service,
  );

  /*
  $form['shipping_zone_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping zone description'),
    '#required' => false,
    '#default_value' => !empty($entity->shipping_service_description) ? $entity->shipping_service_description : '',
  );
  */

  $form['shipping_catchall_zone'] = array(
    '#type' => 'checkbox',
    '#title' => t('Catch-all zone'),
    '#description' => t('If an address fails to match a zone, then catch it here.'),
    '#required' => FALSE,
    '#default_value' => !empty($entity->shipping_catchall_zone) ? $entity->shipping_catchall_zone : '',
  );

  // Load entity in form.
  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );
  // Attach fields by this entity.
  field_attach_form('shipping_matrix_zone', $entity, $form, $form_state);

  $language = $form['shipping_matrix_locality']['#language'];
  $delta = $form['shipping_matrix_locality'][$language]['#max_delta'];
  // Provide an empty field on new zones, but remove the empty one when editing
  // existing zones.
  if ($delta != 0) {
    $form['shipping_matrix_locality'][$language]['#max_delta'] = $delta - 1;
    unset($form['shipping_matrix_locality'][$language][$delta]);
  }

  // Attach buttons to this form.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 200,
  );

  return $form;
}


/**
 * Validation handler for entity_example_basic_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function commerce_shipping_matrix_ui_zones_form_validate($form, &$form_state) {
  //field_attach_form_validate('shipping_matrix_zones', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits basic_add_form information.
 */
function commerce_shipping_matrix_ui_zones_form_submit($form, &$form_state) {
  // Delete button clicked, rebuild the form to display confirmation prompt.
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['ask_confirm'] = TRUE;
  }
  // Deletion confirmed.
  elseif (isset($form_state['values']['confirm']) && $form_state['values']['confirm']) {
    commerce_shipping_matrix_ui_zones_delete($form, $form_state);
  }
  // Save the zone.
  else {
    $entity = $form_state['values']['basic_entity'];
    $entity->shipping_zone_name = $form_state['values']['shipping_zone_name'];
    $entity->shipping_catchall_zone = $form_state['values']['shipping_catchall_zone'];
    $entity->shipping_zone_shipping_service_id = $form_state['values']['shipping_service'];

    //$entity->shipping_service_description = $form_state['values']['shipping_service_description'];
    field_attach_submit('shipping_matrix_zone', $entity, $form, $form_state);
    drupal_set_message(t('Submitted'));
    $entity = commerce_shipping_matrix_ui_zones_save($entity);
    $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix/' . $form_state['values']['shipping_service'] . '/zones';
  }
}

function commerce_shipping_matrix_ui_zones_save(&$entity) {
  return entity_get_controller('shipping_matrix_zone')->save($entity);
}

/**
 * Delete function for zones form submit.
 */
function commerce_shipping_matrix_ui_zones_delete($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  $shipping_service = $entity->shipping_zone_shipping_service_id;
  $entity = commerce_shipping_matrix_ui_zones_del($entity);
  drupal_set_message(t('Shipping zone deleted'));
  $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix/' . $shipping_service . '/zones';
}

/**
 * Delete shipping zone entity.
 */
function commerce_shipping_matrix_ui_zones_del(&$entity) {
  return entity_get_controller('shipping_matrix_zone')->delete($entity);
}
