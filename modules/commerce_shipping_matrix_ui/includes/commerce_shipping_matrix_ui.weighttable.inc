<?php

/**
 * Returns a administration page for Matrix Shipping services.
 */
function commerce_shipping_matrix_ui_weight_table_admin_page($shipping_service, $zone) {
  $content = array();
  $content['table'] = commerce_shipping_matrix_ui_weight_table_list_entities($shipping_service, $zone);

  return $content;
}

/**
 * Returns a render array with all Matrix Shipping services.
 */
function commerce_shipping_matrix_ui_weight_table_list_entities($shipping_service, $zone) {
  $content = array();

  // Load all shipping matrix shipping services.
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'shipping_matrix_weight_table')
                    //->entityCondition('bundle', 'shipping_matrix_zone')
                    ->propertyCondition('weight_table_zone_id', $shipping_service)
                    //->fieldCondition('field_date', 'value', array('2011-03-01', '2011-03-31'), 'BETWEEN')
                    //->fieldOrderBy('field_date', 'value', 'ASC')
                    ->execute();



  if (!empty($entities)) {
    $entities = commerce_shipping_matrix_ui_weight_table_load_multiple(array_keys($entities['shipping_matrix_weight_table']));

    foreach ($entities as $entity) {
      $entity_wrapper = entity_metadata_wrapper('shipping_matrix_weight_table', $entity);

      $ops[0] = array(
        'data' => array(
          'edit' => l(t('edit'), 'admin/commerce/config/shipping/shipping-matrix/' . $shipping_service . '/' . $zone . '/' . $entity->weight_table_id . '/edit'),
        ),
        'no_striping' => true
      );
      $operations = array(
        '#theme' => 'table',
        '#rows' => $ops
      );

      $rows[] = array(
        'data' => array(
          'id' => $entity->weight_table_id,
          'weight' => isset($entity->shipping_matrix_weight) ? physical_weight_format($entity_wrapper->shipping_matrix_weight->value()) : '',
          'weight_gross' => isset($entity->shipping_matrix_weight_gross) ? physical_weight_format($entity_wrapper->shipping_matrix_weight_gross->value()) : '',
          'weight_price' =>  isset($entity->shipping_matrix_price) ? commerce_currency_format($entity_wrapper->shipping_matrix_price->value()['amount'], $entity_wrapper->shipping_matrix_price->value()['currency_code']) : '',
          'operations' =>  drupal_render($operations),
        ),
      );
    }
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Net Weight'), t('Gross Weight'), t('Weight-based shipping cost'), t('Operations')),
    );
  }
  else {
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No weight rows currently exist.'),
    );
  }
  return $content;
}

function commerce_shipping_matrix_ui_weight_table_load_multiple($basic_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('shipping_matrix_weight_table', $basic_ids, $conditions, $reset);
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function commerce_shipping_matrix_ui_weight_table_edit($shipping_service, $zone, $table) {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  return drupal_get_form('commerce_shipping_matrix_ui_weight_table_form', $table, $shipping_service, $zone);
}



/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function commerce_shipping_matrix_ui_weight_table_add($shipping_service, $zone) {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('shipping_matrix_weight_table')->create();
  return drupal_get_form('commerce_shipping_matrix_ui_weight_table_form', $entity, $shipping_service, $zone);
}

/**
 * Form function to create an entity_example_basic entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function commerce_shipping_matrix_ui_weight_table_form($form, &$form_state, $entity, $shipping_service = false, $zone = false) {
  // Load entity by id, if entity is passed as id.
  if (!is_object($entity) && ctype_digit($shipping_service) && ctype_digit($zone)) {
    $entities = commerce_shipping_matrix_ui_weight_table_load_multiple(array($entity));
    if (empty($entities[$entity])) {
      drupal_set_message(t('Weight table %id not found', array('%id' => $entity)));
      drupal_goto('admin/commerce/config/shipping/shipping-matrix');
      return;
    }
    $entity = $entities[$entity];
  }

  $entity->weight_table_shipping_id = !empty($entity->weight_table_shipping_id) ? $entity->weight_table_shipping_id : $shipping_service;
  $entity->weight_table_zone_id = !empty($entity->weight_table_zone_id) ? $entity->weight_table_zone_id : $zone;

  // Load entity in form.
  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  // Confirm deletion.
  if (isset($form_state['storage']) && $form_state['storage']['ask_confirm'] ) {
    $form['basic_entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    $question = t('Are you sure you want to delete this weight table?');
    $path = current_path();
    $description = t('This cannot be undone.');
    $yes = t('Confirm');
    $no = t('Cancel');
    return confirm_form($form, $question, $path, $description, $yes, $no);
  }


  // Not deleting the service, show the standard form.
  // Attach fields by this entity.
  field_attach_form('shipping_matrix_weight_table', $entity, $form, $form_state);


  // Attach buttons to this form.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 200,
  );

  return $form;
}


/**
 * Validation handler for entity_example_basic_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function commerce_shipping_matrix_ui_weight_table_form_validate($form, &$form_state) {
  //field_attach_form_validate('shipping_matrix_ui_weight_table', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits basic_add_form information
 */
function commerce_shipping_matrix_ui_weight_table_form_submit($form, &$form_state) {
  // Delete button clicked, rebuild the form to display confirmation prompt.
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['ask_confirm'] = TRUE;
  }
  // Deletion confirmed.
  elseif (isset($form_state['values']['confirm']) && $form_state['values']['confirm']) {
    commerce_shipping_matrix_ui_weight_table_delete($form, $form_state);
  }
  // Save the service.
  else {
    $entity = $form_state['values']['basic_entity'];
    //$entity->weight_table_zone_id = $entity->weight_table_zone_id;
    field_attach_submit('shipping_matrix_weight_table', $entity, $form, $form_state);

    drupal_set_message(t('Submitted'));
    $entity = commerce_shipping_matrix_ui_weight_table_save($entity);
    $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix/' . $entity->weight_table_shipping_id . '/zones-table/' . $entity->weight_table_zone_id;
  }
}

function commerce_shipping_matrix_ui_weight_table_save(&$entity) {
  return entity_get_controller('shipping_matrix_weight_table')->save($entity);
}

/**
 * Delete function for form submit
 */
function commerce_shipping_matrix_ui_weight_table_delete($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix/' . $entity->weight_table_shipping_id . '/zones-table/' . $entity->weight_table_zone_id;
  $entity = commerce_shipping_matrix_ui_weight_table_del($entity);
  drupal_set_message(t('Shipping service deleted'));
}

/**
 * Delete shipping service entity
 */
function commerce_shipping_matrix_ui_weight_table_del(&$entity) {
  return entity_get_controller('shipping_matrix_weight_table')->delete($entity);
}
