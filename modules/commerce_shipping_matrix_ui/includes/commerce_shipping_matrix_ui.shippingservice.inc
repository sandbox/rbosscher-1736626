<?php

/**
 * Returns a administration page for Matrix Shipping services.
 */
function commerce_shipping_matrix_ui_shippingservice_admin_page() {
  $content = array();
  $content['table'] = commerce_shipping_matrix_ui_shippingservice_list_entities();
  return $content;
}

/**
 * Returns a render array with all Matrix Shipping services.
 */
function commerce_shipping_matrix_ui_shippingservice_list_entities() {
  $content = array();

  // Load all shipping matrix shipping services.
  $entities = commerce_shipping_matrix_ui_shippingservice_load_multiple();

  if (!empty($entities)) {
    foreach ($entities as $entity) {
      // Set operations for this shipping service
      $ops[0] = array(
        'data' => array(
          'edit' => l(t('edit shipping service'), 'admin/commerce/config/shipping/shipping-matrix/' . $entity->shipping_service_id . '/edit'),
          'zone' => l(t('zone overview'), 'admin/commerce/config/shipping/shipping-matrix/' . $entity->shipping_service_id . '/zones'),
        ),
        'no_striping' => true
      );
      $operations = array(
        '#theme' => 'table',
        '#rows' => $ops
      );
      $entity_wrapper = entity_metadata_wrapper('shipping_matrix_services', $entity);
      $price = $entity_wrapper->shipping_matrix_price->value();
      $dimensions = $entity_wrapper->shipping_matrix_dimensions->value();
      $weight = $entity_wrapper->shipping_matrix_weight->value();
      $rows[] = array(
        'data' => array(
          'id' => $entity->shipping_service_id,
          'item_description' => $entity->shipping_service_name,
          'zones' => $entity->shipping_service_id,
          'default_rate' => !empty($price) ? commerce_currency_format($price['amount'], $price['currency_code']) : '',
          'max_dimensions' => !empty($dimensions) ? physical_dimensions_format($dimensions) : '',
          'max_weight' => !empty($weight) ? physical_weight_format($weight) : '',
          'operations' => drupal_render($operations),
        ),
      );
    }
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Shipping Service'), t('Zones'), t('Basic Shipping Rate'), t('Max Dimensions'), t('Max Weight'), t('Opperations')),
    );
  }
  else {
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No Matrix Shipping Service currently exist.'),
    );
  }
  return $content;
}

function commerce_shipping_matrix_ui_shippingservice_load_multiple($basic_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('shipping_matrix_services', $basic_ids, $conditions, $reset);
}


/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function commerce_shipping_matrix_ui_shippingservice_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('shipping_matrix_services')->create();
  return drupal_get_form('commerce_shipping_matrix_ui_shippingservice_form', $entity);
}

/**
 * Form function to create an entity_example_basic entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function commerce_shipping_matrix_ui_shippingservice_form($form, &$form_state, $entity) {
  // Load entity by id, if entity is passed as id.
  if (!is_object($entity) && ctype_digit($entity)) {
    $entities = commerce_shipping_matrix_ui_shippingservice_load_multiple(array($entity));
    if (empty($entities[$entity])) {
      drupal_set_message(t('Shipping service %id not found', array('%id' => $entity)));
      drupal_goto('admin/commerce/config/shipping/shipping-matrix');
      return;
    }
    $entity = $entities[$entity];
  }

  // Confirm deletion.
  if (isset($form_state['storage']) && $form_state['storage']['ask_confirm'] ) {
    $form['basic_entity'] = array(
      '#type' => 'value',
      '#value' => $entity,
    );
    $question = t('Are you sure you want to delete this shipping service?');
    $path = current_path();
    $description = t('This cannot be undone.');
    $yes = t('Confirm');
    $no = t('Cancel');
    return confirm_form($form, $question, $path, $description, $yes, $no);
  }


  // Not deleting the service, show the standard form.
  $form['shipping_service_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping Service Name'),
    '#required' => TRUE,
    '#default_value' => !empty($entity->shipping_service_name) ? $entity->shipping_service_name : '',
  );
  $form['shipping_service_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping Service Description'),
    '#required' => false,
    '#default_value' => !empty($entity->shipping_service_description) ? $entity->shipping_service_description : '',
  );

  // Load entity in form.
  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );
  // Attach fields by this entity.
  field_attach_form('shipping_matrix_services', $entity, $form, $form_state);

  // Attach buttons to this form.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 200,
  );

  return $form;
}


/**
 * Validation handler for entity_example_basic_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function commerce_shipping_matrix_ui_shippingservice_form_validate($form, &$form_state) {
  //field_attach_form_validate('shipping_matrix_ui_shippingservice', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits basic_add_form information
 */
function commerce_shipping_matrix_ui_shippingservice_form_submit($form, &$form_state) {
  // Delete button clicked, rebuild the form to display confirmation prompt.
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['ask_confirm'] = TRUE;
  }
  // Deletion confirmed.
  elseif (isset($form_state['values']['confirm']) && $form_state['values']['confirm']) {
    commerce_shipping_matrix_ui_shippingservice_delete($form, $form_state);
  }
  // Save the service.
  else {
    $entity = $form_state['values']['basic_entity'];
    $entity->shipping_service_name = $form_state['values']['shipping_service_name'];
    $entity->shipping_service_description = $form_state['values']['shipping_service_description'];
    field_attach_submit('shipping_matrix_services', $entity, $form, $form_state);
    drupal_set_message(t('Submitted'));

    $entity = commerce_shipping_matrix_ui_shippingservice_save($entity);
    $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix';
  }
}

function commerce_shipping_matrix_ui_shippingservice_save(&$entity) {
  $controller = entity_get_controller('shipping_matrix_services')->save($entity);

  // Clear caches and rebuild the menu items.
  commerce_shipping_services_reset();
  entity_defaults_rebuild();
  rules_clear_cache(TRUE);
  menu_rebuild();

  return $controller;
}

/**
 * Delete function for form submit.
 */
function commerce_shipping_matrix_ui_shippingservice_delete($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  $entity = commerce_shipping_matrix_ui_shippingservice_del($entity);
  drupal_set_message(t('Shipping service deleted'));
  $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix';
}

/**
 * Delete shipping service entity
 */
function commerce_shipping_matrix_ui_shippingservice_del(&$entity) {
  return entity_get_controller('shipping_matrix_services')->delete($entity);
}
