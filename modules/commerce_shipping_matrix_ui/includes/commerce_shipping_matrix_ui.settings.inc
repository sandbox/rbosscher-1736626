<?php


/**
 * Form function to create an entity_example_basic entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function commerce_shipping_matrix_ui_settings_form($form, &$form_state) {
  $form['#tree'] = TRUE;  // Prevent flattening the form values
  // addressfield implementation:
  // Get default fields
  // @TODO Maybe get also user defined fields??

  $form['shipping_matrix_addressfield_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Addressfield Settings'),
    '#description' => t('Select here the addressfield(s) the shipping calculator needs to check for in a zone'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );


  $addressfield_settings = variable_get('shipping_matrix_addressfield_settings', array());
  $available_countries = _addressfield_country_options_list();
  $addressfields = addressfield_default_values($available_countries, '', array());
  foreach ($addressfields as $fieldname => $defaultvalue) {
    // Make county mandatory
    // We atleast want this value, Addressfields considers every address field empty if it has no country
    if ($fieldname == 'country') {
      $form['shipping_matrix_addressfield_settings'][$fieldname] = array(
        '#type' => 'checkbox',
        '#title' => t($fieldname),
        '#default_value' => (!empty($addressfield_settings[$fieldname]))
                            ? $addressfield_settings[$fieldname]
                            : TRUE,
        '#disabled' => TRUE,
        '#description' => t('The country must be enabled because of the nature of the addressfield'),
      );
    }
    else {
      $form['shipping_matrix_addressfield_settings'][$fieldname] = array(
        '#type' => 'checkbox',
        '#title' => t($fieldname),
        '#default_value' => (!empty($addressfield_settings[$fieldname]))
                            ? $addressfield_settings[$fieldname]
                            : FALSE,
      );
    }
  }


  $form['shipping_matrix_calculation_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calculation Settings'),
    '#description' => t('Here you can change some preferences in calculation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );


  $shipping_matrix_calculation_settings = variable_get('shipping_matrix_calculation_settings', array());

  $form['shipping_matrix_calculation_settings']['shipping_matrix_zone_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Zone handling'),
    '#default_value' => (!empty($shipping_matrix_calculation_settings['shipping_matrix_zone_sort']))
                        ? $shipping_matrix_calculation_settings['shipping_matrix_zone_sort']
                        : 'min',
    '#options' => array('min' => t('Zone with the least price amount'), 'max' => t('Zone with the most price amount')),
    '#description' => t('At shipping calculation, we can choose to charge the cheapest zone or the most expensive one.'),
  );

  $form['shipping_matrix_calculation_settings']['shipping_matrix_weighttable_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Weight table handling'),
    '#default_value' => (!empty($shipping_matrix_calculation_settings['shipping_matrix_weighttable_sort']))
                        ? $shipping_matrix_calculation_settings['shipping_matrix_weighttable_sort']
                        : 'min',
    '#options' => array('min' => t('Weight row with the least price amount'), 'max' => t('Weight row with the most price amount')),
    '#description' => t('At shipping calculation, we can choose to charge the cheapest weight row or the most expensive one.'),
  );


  $form['shipping_matrix_calculation_settings']['shipping_matrix_net_gross'] = array(
    '#type' => 'radios',
    '#title' => t('Weight field used for calculations'),
    '#default_value' => (!empty($shipping_matrix_calculation_settings['shipping_matrix_net_gross']))
                        ? $shipping_matrix_calculation_settings['shipping_matrix_net_gross']
                        : 'net',
    '#options' => array('net' => t('Netto weight field'), 'gross' => t('Gross weight field')),
    '#description' => t('At shipping calculation, we can use netto weights or gross weights for calculation.'),
  );

  $form['shipping_matrix_calculation_settings']['shipping_matrix_zone_handling'] = array(
    '#type' => 'radios',
    '#title' => t('Shipping services without zones'),
    '#default_value' => (!empty($shipping_matrix_calculation_settings['shipping_matrix_zone_handling']))
                        ? $shipping_matrix_calculation_settings['shipping_matrix_zone_handling']
                        : 'disabled',
    '#options' => array('disabled' => t('Disable shippingservice when no zones are applicable'), 'enabled' => t('Enable shipping service with no applicable zones')),
    '#description' => t('If you enabled this, the shipping servie rate will be applied even if there are no zones returned.'),
  );


  //drupal_set_message(print_r($shipping_matrix_calculation_settings, true), 'warning');

  return system_settings_form($form);
}


/**
 * Validation handler for entity_example_basic_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function commerce_shipping_matrix_ui_settings_form_validate($form, &$form_state) {
  //field_attach_form_validate('shipping_matrix_ui_shippingservice', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits basic_add_form information
 */
function commerce_shipping_matrix_ui_settings_form_submit($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  $entity->shipping_service_name = $form_state['values']['shipping_service_name'];
  $entity->shipping_service_description = $form_state['values']['shipping_service_description'];
  field_attach_submit('shipping_matrix_services', $entity, $form, $form_state);
  drupal_set_message(t('Submitted'));
  $entity = commerce_shipping_matrix_ui_shippingservice_save($entity);
  $form_state['redirect'] = 'admin/commerce/config/shipping/shipping-matrix';
}
