<?php
/**
 * @file
 * Defines an shipping method for shipping by weight and country.
 */

// include entity controlers
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'commerce_shipping_matrix') . '/commerce_shipping_matrix.controllers.inc';



/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_shipping_matrix_commerce_shipping_method_info() {
  $shipping_methods = array();
  $shipping_methods['shipping_matrix'] = array(
    'title' => t('Shipping Matrix'),
    'description' => t('Shipping quote from shipping matrix.'),
  );
  return $shipping_methods;
}



/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_shipping_matrix_commerce_shipping_service_info() {
  $shipping_services = array();
  $available_services = _commerce_shipping_matrix_service_list();
  $selected_services = variable_get('shipping_matrix_services', array());
  foreach ($available_services as $id => $val) {
    if ($val != 0) { // if you find a selected one...
      $service = $available_services[$id];
      $shipping_services[$service['slug']] = array(
        'title' => t($service['title']),
        'description' => t($service['description']),
        'display_title' => t($service['title']),
        'shipping_method' => 'shipping_matrix',
        'price_component' => 'shipping',
        'callbacks' => array(
          'rate' => 'commerce_shipping_matrix_service_rate',
        ),
      );
    }
  }
  return $shipping_services;
}



/**
 * Function to get the created services services.
 *
 * @return
 *   an array populated with the shipping_service_id
 *   or incase empty an empry array
 */
function _commerce_shipping_matrix_service_list() {
  $list = entity_load('shipping_matrix_services');
  $services = array();
  foreach ($list as $key => $service) {
  $services[$service->shipping_service_id] = array(
    'shipping_service_id' => $service->shipping_service_id,
    'title' => t($service->shipping_service_name),
    'description' => t($service->shipping_service_description),
    );
  }
 // Make a unique ID to identify the service by
  foreach ($services as $key => $service) {
    $service['slug'] = 'shipping_matrix_service_id_' . $service['shipping_service_id'];
    $services[$key] = $service;
  }
  return $services;
}



/**
 * Function for retreiving the raw shipping service id
 *
 * @param $service
 *   The shipping service id name
 *
 * @return
 *   The raw numeric shipping service id;
 *   FALSE if service raw value is not numeric
 */
function commerce_shipping_matrix_get_shipping_service_name($service) {
  if (is_array($service)) {
    $service = $service['base'];
  }
  $service = str_replace('shipping_matrix_service_id_', '', $service);
  if (ctype_digit($service)) {
    return $service;
  }
  return FALSE;
}



/**
 * Loads the specified shipping service entity.
 *
 * @param $shipping_service
 *   The shipping service id name
 *
 * @return
 *   a loaded shipping service entity
 */
function commerce_shipping_matrix_get_shipping_service($shipping_service) {
  $shipping_service = commerce_shipping_matrix_get_shipping_service_name($shipping_service);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'shipping_matrix_services')
    ->propertyCondition('shipping_service_id', $shipping_service);
  $result = $query->execute();

  if (isset($result['shipping_matrix_services'])) {
    $shipping_matrix_services_ids = array_keys($result['shipping_matrix_services']);
    $shipping_matrix_services = entity_load('shipping_matrix_services', $shipping_matrix_services_ids);
  }
  return $shipping_matrix_services;
}



/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 *
 * @param $shipping_service
 *   The shipping service id name
 * @param $order
 *   The order object
 *
 * @return
 *   The amount as array 'amount', 'currency_code', 'data'
 *   FALSE if amount is 0 or smaller
 */
function commerce_shipping_matrix_service_rate($shipping_service, $order) {
  $shipping_matrix_services = commerce_shipping_matrix_get_shipping_service($shipping_service);
  $key = key($shipping_matrix_services); // Returns the first key of it
  $matrix_service_wrapper = entity_metadata_wrapper('shipping_matrix_services', $shipping_matrix_services[$key]);

  $shipping_matrix_price = $matrix_service_wrapper->shipping_matrix_price->value();
  //@TODO Maybe some sort options, cheapest ASC DESC, zone name ASC DESC, zone id ASC DESC?

  // use shipping service as base currency
  // setup componets to calculate total.
  $components = $shipping_matrix_price;
  $components['data']['components'][]['price'] = $shipping_matrix_price;

  // Get the cost of the zone.
  $zone_costs = _commerce_shipping_matrix_get_zone_cost($shipping_service, $order);
  $calculation_settings = variable_get('shipping_matrix_calculation_settings', array());

  // When we do not what the shipping service to be applicable if there are no
  // matching zones.
  if (empty($calculation_settings['shipping_matrix_zone_handling'])
    || $calculation_settings['shipping_matrix_zone_handling'] != 'enabled') {
    if (empty($zone_costs)) {
      return array();
    }
  }


  // If zone has costs add it to the components.
  if (!empty($zone_costs)) {
    //return FALSE;
    //add zone cost to the components
    foreach ($zone_costs['data']['components'] as $component) {
      $components['data']['components'][] = $component;
    }
  }


  $shipping_costs = commerce_price_component_total($components);

  //if amount has a value return the amount
  if ($shipping_costs['amount'] > 0) {
    return array(
      'amount' => $shipping_costs['amount'],
      'currency_code' => $shipping_costs['currency_code'],
      'data' => $shipping_costs['data'],
    );
  }
  return FALSE;
}



// function for retreiving the shipping country of an order
function _commerce_shipping_matrix_get_shipping_address($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $profile = $order_wrapper->commerce_customer_shipping->value();
  if (empty($profile)) {
    return;
  }
  $profile_entity = commerce_customer_profile_load_multiple(array($profile->profile_id));
  $key = key($profile_entity);
  $profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile_entity[$key]);
  return $profile_wrapper->commerce_customer_address->value();
}



// function for retreiving the zones that are available for a country
function _commerce_shipping_matrix_get_zone_cost($service_id, $order) {
  // We need to make sure all calculataions in weight are in the same unit.
  // So we setup a standard unit, i have chosen for grams.
  $unit = 'g';
  $components = array();

  //@todo needs to change in addressfield... for comparing against other address fields
  $shipping_address = _commerce_shipping_matrix_get_shipping_address($order);
  $country = $shipping_address['country'];
  $locality = $shipping_address['locality'];
  $postal_code = $shipping_address['postal_code'];

  // Total order weight.
  $total_order_weight = commerce_physical_order_weight($order, $unit);
  $calculation_settings = variable_get('shipping_matrix_calculation_settings', array());

  // Get the zone information
  //@todo change field condition to addressfield
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'shipping_matrix_zone')
    ->propertyCondition('shipping_zone_shipping_service_id', commerce_shipping_matrix_get_shipping_service_name($service_id))
    ->fieldCondition('shipping_matrix_locality', 'country', $country, '=');
  $result = $query->execute();

  // No matching zone, see if we have a catchall zone.
  if (empty($result['shipping_matrix_zone'])) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'shipping_matrix_zone')
      ->propertyCondition('shipping_zone_shipping_service_id', commerce_shipping_matrix_get_shipping_service_name($service_id))
      ->propertyCondition('shipping_catchall_zone', 1);
    $result = $query->execute();
  }

  // get zone entity
  if (isset($result['shipping_matrix_zone'])) {
    $shipping_matrix_zone_ids = array_keys($result['shipping_matrix_zone']);
    $shipping_matrix_zone = entity_load('shipping_matrix_zone', $shipping_matrix_zone_ids);
    reset($shipping_matrix_zone); //Safety - sets pointer to top of array
    $key = key($shipping_matrix_zone); // Returns the first key of it
    $zone_wrapper = entity_metadata_wrapper('shipping_matrix_zone', $shipping_matrix_zone[$key]);

    // Get and set the zone price
    if (!empty($zone_wrapper->shipping_matrix_price->value())) {
      $components = $zone_wrapper->shipping_matrix_price->value();
      $components['data']['components'][]['price'] = $zone_wrapper->shipping_matrix_price->value();
    }

    // Get the table weight quote.

    //@TODO: add sorting support multi-currency!!
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'shipping_matrix_weight_table')
      ->propertyCondition('weight_table_zone_id', $shipping_matrix_zone[$key]->shipping_zone_id, '=')
      ->fieldOrderBy('shipping_matrix_price', 'amount', 'ASC');

    $result = $query->execute();

    if (isset($result['shipping_matrix_weight_table'])) {
      $weight_table_ids = array_keys($result['shipping_matrix_weight_table']);
      $weight_table = entity_load('shipping_matrix_weight_table', $weight_table_ids);

      foreach ($weight_table as $row => $weight_option) {
        $weight_wrapper = entity_metadata_wrapper('shipping_matrix_weight_table', $weight_option);
        $tableweight_net = physical_weight_convert($weight_wrapper->shipping_matrix_weight->value(), $unit);
        $tableweight_gross = physical_weight_convert($weight_wrapper->shipping_matrix_weight_gross->value(), $unit);

        // Unset the non-applicable table weights for net weights.
        if (empty($calculation_settings['shipping_matrix_net_gross']) || $calculation_settings['shipping_matrix_net_gross'] != 'gross') {
          if ($tableweight_net['weight'] < $total_order_weight['weight']) {
            unset($weight_table[$row]);
          }
          else {
            $numbers[$row] = $weight_wrapper->shipping_matrix_price->value()['amount'];
          }
        }

        // Unset the non-applicable table weights for gross weights.
        else {
          if ($tableweight_gross['weight'] < $total_order_weight['weight']) {
            unset($weight_table[$row]);
          }
          else {
            $numbers[$row] = $weight_wrapper->shipping_matrix_price->value()['amount'];
          }
        }
      }

      if (!empty($numbers)) {
        // Get the key for the table row with the least amount.
        if (empty($calculation_settings['shipping_matrix_weighttable_sort']) || $calculation_settings['shipping_matrix_weighttable_sort'] != 'max') {
          $key = array_search(min($numbers), $numbers);
        }

        // Get the key for the table row with the max amount.
        else {
          $key = array_search(max($numbers), $numbers);
        }

        // Add weight table price component to total.
        $selected_weight_wrapper = entity_metadata_wrapper('shipping_matrix_weight_table', $weight_table[$key]);
        $components['data']['components'][]['price'] = $selected_weight_wrapper->shipping_matrix_price->value();
      }

    }
  }


  // Combine all components to one price.
  if (!empty($components)) {
    // Return total price amount.
    return $components;
  }

  return FALSE;
}

/**
 * Determines the maximum weight for this shipping service.
 *
 * @param $shipping_service
 *   The shipping service id name
 *
 * @return
 *   a physical field (weight) array with $max_weight['weight'] and $max_weight['unit']
 *   the specified unit of measurement or empty array if no weight could be determined.
 */
function commerce_shipping_matrix_get_max_weight_shipping_service($shipping_service) {
  $shipping_matrix_services = commerce_shipping_matrix_get_shipping_service($shipping_service);
  reset($shipping_matrix_services); //Safety - sets pointer to top of array
  $key = key($shipping_matrix_services); // Returns the first key of it
  
  $service_wrapper = entity_metadata_wrapper('shipping_matrix_services', $shipping_matrix_services[$key]);

  // get max weight for this shipping service
  $max_weight = array();
  $max_weight = $service_wrapper->shipping_matrix_weight->value();
  return $max_weight;
}



/**
 * Determines the maximum dimensions for this shipping service.
 *
 * @param $shipping_service
 *   The shipping service id name
 *
 * @return
 *   a physical field (dimension) array with $outerbox['length']
 *   $outerbox['width'] $outerbox['height'] and $outerbox['unit']
 *   the specified unit of measurement or empty array if no dimension
 *   could be determined.
 */
function commerce_shipping_matrix_get_max_dimensions_shipping_service($shipping_service) {
  $shipping_matrix_services = commerce_shipping_matrix_get_shipping_service($shipping_service);
  reset($shipping_matrix_services); //Safety - sets pointer to top of array
  $key = key($shipping_matrix_services); // Returns the first key of it

  $service_wrapper = entity_metadata_wrapper('shipping_matrix_services', $shipping_matrix_services[$key]);

  // get max dimension for this shipping service
  $outerbox = array();
  $outerbox = $service_wrapper->shipping_matrix_dimensions->value();
  return $outerbox;
}



/**
 * Shipping service callback: returns the example shipping service details form.
 */
function commerce_shipping_matrix_service_details_form($pane_form, $pane_values, $checkout_pane, $order, $shipping_service) {
  $form = array();
  $pane_values['service_details'] += array(
    'name' => '',
    'express' => '',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('This is a demonstration field coded to fail validation for single character values.'),
    '#default_value' => $pane_values['service_details']['name'],
    '#required' => TRUE,
  );
  $form['express'] = array(
    '#type' => 'checkbox',
    '#title' => t('Express delivery'),
    '#description' => t('Express delivery (costs an additional E15)'),
    '#default_value' => $pane_values['service_details']['express'],
  );
  return $form;
}



/**
 * Shipping service callback: validates the example shipping service details.
 */
function commerce_shipping_matrix_service_details_form_validate($details_form, $details_values, $shipping_service, $order, $form_parents) {
  if (drupal_strlen($details_values['name']) < 2) {
    form_set_error(implode('][', array_merge($form_parents, array('name'))), t('You must enter a name two or more characters long.'));

    // Even though the form error is enough to stop the submission of the form,
    // it's not enough to stop it from a Commerce standpoint because of the
    // combined validation / submission going on per-pane in the checkout form.
    return FALSE;
  }
}



/**
 * Shipping service callback: increases the shipping line item's unit price if
 * express delivery was selected.
 */
function commerce_shipping_matrix_service_details_form_submit($details_form, $details_values, $line_item) {
  if ($details_values['express']) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    // Build a price array for the express delivery fee.
    $express_price = array(
      'amount' => 1500,
      'currency_code' => 'USD',
      'data' => array(),
    );

  }
}


/**
 * Implements hook_commerce_price_component_type_info().
 */
function commerce_shipping_matrix_commerce_price_component_type_info() {
  return array(
    'example_shipping_service_express' => array(
      'title' => t('Express delivery'),
      'weight' => 20,
    ),
  );
}



/**
 * Implements hook_entity_info().
 */
function commerce_shipping_matrix_entity_info() {
  return array(
    'shipping_matrix_services' => array(
      'label' => t('Shipping Matrix Shipping Service'),
      'base table' => 'shipping_matrix_shipping_service',
      'entity keys' => array(
        'id' => 'shipping_service_id',
      ),
      'fieldable' => TRUE,
      'controller class' => 'ShippingMatrixShippingServiceController',
    ),
    'shipping_matrix_zone' => array(
      'label' => t('Shipping Matrix Zone'),
      'base table' => 'shipping_matrix_shipping_zone',
      'entity keys' => array(
        'id' => 'shipping_zone_id',
      ),
      'fieldable' => TRUE,
      'controller class' => 'ShippingMatrixZonesController',
    ),
    'shipping_matrix_weight_table' => array(
      'label' => t('Shipping Matrix Weight Table'),
      'base table' => 'shipping_matrix_weight_table',
      'entity keys' => array(
        'id' => 'weight_table_id',
      ),
      'fieldable' => TRUE,
      'controller class' => 'ShippingMatrixTableWeightController',
    ),
  );
}
