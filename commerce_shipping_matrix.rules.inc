<?php
/**
 * @file
 * Defined rules for shipping matrix
 */

/**
 * Determines the weight of an entire order.
 *
 * @param $max_weight
 *   A weight field value array representing the total weight of a package in this service
 * @param $order
 *   The order object whose weight value should be calculated.
 * @param $unit
 *   The unit of measurement to use for the returned weight of the order.
 *
 * @return
 *   true if all items of 
 *   the specified unit of measurement or NULL if no weight could be determined.
 */
function commerce_shipping_matrix_order_max_weight($max_weight, $order, $unit = 'g') {
  $max_weight = physical_weight_convert($max_weight, $unit);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // var to check if at leas one items has weight
  $ihm = FALSE;
  // var to check if all items are smaller than the max weight
  $smw = TRUE;
  // Loop over each line item on the order.
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    // Get the weight value of product line items.
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $line_item_weight = commerce_physical_product_line_item_weight($line_item_wrapper->value());
      // Add it to the running total converting it to the required weight unit.
      if (!empty($line_item_weight['weight'])) {
      $ihm = TRUE;
        $converted_weight = physical_weight_convert($line_item_weight, $unit);
    #drupal_set_message($max_weight['weight'] . ' < ' . $converted_weight['weight']);
    if ($max_weight['weight'] < $converted_weight['weight']) {
      $smw = FALSE;
    }
      }
    }
  }
 // both needs to be true, at least one item has to have weight applied to it, and all items needs to be smallerd than the max allowed in a package.
  if ($ihm === TRUE && $smw == TRUE) {
  return TRUE;
  }
  return FALSE; 
  }



/**
 * Checks if a box fits in another box.
 *
 * @param $outerbox
 *   A dimension field value array including the 'length', 'width', 'height' and
 *   'unit'.
 * @param $innerbox
 *   A dimension field value array including the 'length', 'width', 'height' and
 *   'unit'.
 *
 * @return
 *   True is the innerbox fits inside the outerbox
 */
function commerce_shipping_matrix_fits_dimension($outerbox, $innerbox, $unit = 'mm') { 
  
  // Convert dimensions to mm
  $outerbox = physical_dimensions_convert($outerbox, $unit);
  $innerbox = physical_dimensions_convert($innerbox, $unit);
  
  // Class to check if box fits in box
  require_once('commerce_shipping_matrix.binpacking.inc');
  $binpack = new boxing();
  $binpack -> add_outer_box($outerbox['length'], $outerbox['width'], $outerbox['height']); // our quantum box; l, w, h
  $binpack -> add_inner_box($innerbox['length'], $innerbox['width'], $innerbox['height']); // schroedingers cat; l, w, h
  
  // if box fits in box return true
  try {
   //@TODO undefined notice 
   // suppres error for now
  if (@$binpack -> fits()) return TRUE;
  }  
  catch (Exception $e) {
  watchdog_exception($e);
  }

  return FALSE;
}


//---------------------------------------------

// add shipping matrix values to rules
function commerce_shipping_matrix_rules_data_info() {
  return array(
    'shippinh_matrix' => array(
      'label' => t('Shipping Matrix - Shipping Service Values'),
      'wrap' => TRUE,
      'property info' => _commerce_shipping_matrix_shipping_service_values(),
    ),
  );  
  
}

function _commerce_shipping_matrix_shipping_service_values() {
  return array(
    'type' => array(
      'type' => 'text',
      'label' => t('The category to which this message belongs'),
    ),
    'message' => array(
      'type' => 'text',
      'label' => ('Log message'),
      'getter callback' => 'rules_system_log_get_message',
      'sanitized' => TRUE,
    ),
    'severity' => array(
      'type' => 'integer',
      'label' => t('Severity'),
      'options list' => 'watchdog_severity_levels',
    ),
    'request_uri' => array(
      'type' => 'uri',
      'label' => t('Request uri'),
    ),
    'link' => array(
      'type' => 'text',
      'label' => t('An associated, HTML formatted link'),
    ),
  );
}


//---------------------------------------------



/**
 * Implements hook_rules_condition_info().
 */
function commerce_shipping_matrix_rules_condition_info() {

  $conditions = array();

  

  $conditions['shipping_matrix_rules_order_is_under_weight_limit_shipping_service'] = array(
    'label' => t("Order's total weight is under the shipping service limit"),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
        'shipping_service_name' => array(
          'type' => 'text',
          'label' => t('Shipping service'),
          'options list' => 'commerce_shipping_service_options_list',
        ),

    ),
    'group' => t('Shipping Matrix'),
  );

  $conditions['shipping_matrix_rules_order_maximum_dimension_is_under_maximum_shipping_service'] = array(
    'label' => t("The maximum dimension of any product in the order is under the maximum of the shipping service"),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
        'shipping_service_name' => array(
          'type' => 'text',
          'label' => t('Shipping service'),
          'options list' => 'commerce_shipping_service_options_list',
        ),
    ),
    'group' => t('Shipping Matrix'),
  );



/*
 * THIS CAN BE DELETED AFTER COMMERCE_PHYSICAL IS UPDATED AND APPLIED #18 
 * FROM ISSUE http://drupal.org/node/1344962
 *
 */

$conditions['shipping_matrix_rules_order_is_under_weight_limit'] = array(
    'label' => t("Order's total weight is under a chosen limit"),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'limit' => array(
        'type' => 'decimal',
        'label' => t('Weight limit'),
      ),
      'unit' => array(
        'type' => 'text',
        'label' => t('Weight unit'),
        'options list' => 'physical_weight_unit_options',
        'restriction' => 'input',
        'default value' => 'kg',
      ),
    ),
    'group' => t('Commerce Physical'),
  );


  $conditions['shipping_matrix_rules_order_maximum_dimension_is_under_size'] = array(
    'label' => t("The maximum dimension of any product in the order is under a certain size"),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'limit' => array(
        'type' => 'decimal',
        'label' => t('Size limit'),
      ),
      'unit' => array(
        'type' => 'text',
        'label' => t('Dimension unit'),
        'options list' => 'physical_dimension_unit_options',
        'restriction' => 'input',
        'default value' => 'cm',
      ),
    ),
    'group' => t('Commerce Physical'),
  );

  $conditions['shipping_matrix_rules_order_is_shippable'] = array(
    'label' => t("The order contains shippable products"),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Physical'),
  );

  return $conditions;
}

/**
 * Rules condition: checks to see if the given order's total weight is under the 
 * shipping service limit.
 */
function commerce_shipping_matrix_rules_order_is_under_weight_limit_shipping_service($order, $shipping_service) {
  
  // we need a unit in wich we can compare
  $unit = 'g';
  
  $shipping_weight = commerce_shipping_matrix_get_max_weight_shipping_service($shipping_service);
  $shipping_weight = physical_weight_convert($shipping_weight, $unit);

  //drupal_set_message($shipping_service, 'warning'); 
  $order_weight = commerce_physical_order_weight($order, $unit);


  return $order_weight['weight'] <= $shipping_weight['weight'];
}




function commerce_shipping_matrix_rules_order_maximum_dimension_is_under_maximum_shipping_service($order, $shipping_service) {
  $unit = 'mm';
  // Get max dimension from shipping service
  $max_dimension = commerce_shipping_matrix_get_max_dimensions_shipping_service($shipping_service);  
  $product_dimensions = commerce_shipping_matrix_order_dimensions($order, $unit);
  // Get the dimensions of every product in the order
  foreach ($product_dimensions as $dimension) {
    //drupal_set_message('<pre>'. print_r($dimension, true) . ' is smaller than ' . print_r($max_dimension, true) . '</pre>', 'warning');
    // Check each of length / width / height
    if (commerce_shipping_matrix_fits_dimension($max_dimension, $dimension) === FALSE) {
      return FALSE;
      }
    }
  return TRUE;
  }









/*
 * THIS CAN BE DELETED AFTER COMMERCE_PHYSICAL IS UPDATED AND APPLIED #18 
 *  FROM ISSUE http://drupal.org/node/1344962
 *
 */

function commerce_shipping_matrix_product_line_item_dimensions($line_item) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $dimensions = NULL;

  // If the line item references a valid product...
  if (!empty($line_item_wrapper->commerce_product)) {
    $product = $line_item_wrapper->commerce_product->value();

    if (!empty($product)) {
      // If the product has a valid dimensions field...
      $field_name = commerce_physical_entity_dimensions_field_name('commerce_product', $product);

      if (!empty($field_name) && !empty($product->{$field_name})) {
        // Extract the dimensions value from the product.
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
        $dimensions[] = $product_wrapper->{$field_name}->value();
      }
    }
  }

  // Allow other modules to alter the volume if necessary.

  return $dimensions;
}





/**
 * Rules condition: checks to see if the maximum dimension of any product in the
 * order is under a certain size
 */
function commerce_shipping_matrix_rules_order_maximum_dimension_is_under_size($order, $limit, $unit) {

  $max_dimension = 0;

  $dimension_keys = array(
    'length',
    'width',
    'height',
  );

  // Get the dimensions of every product in the order
  foreach (commerce_shipping_matrix_order_dimensions($order, $unit) as $dimension) {

    // Check each of length / width / height
    foreach ($dimension_keys as $dimension_key) {

      // If this dimension's bigger than the current max, it's the new max.
      if ($dimension[$dimension_key] > $max_dimension) {
        $max_dimension = $dimension[$dimension_key];
      }
    }
  }

  return $max_dimension <= $limit;
}

/**
 * Rules condition: checks to see if the given order's total weight is under a
 * certain limit.
 */
function commerce_shipping_matrix_rules_order_is_under_weight_limit($order, $limit, $unit) {

  $order_weight = commerce_physical_order_weight($order, $unit);

  return $order_weight['weight'] <= $limit;
}


/**
 * Rules condition: check if the order contains shippable products.
 */
function commerce_shipping_matrix_rules_order_is_shippable($order) {
  return commerce_physical_order_shippable($order);
}



function commerce_shipping_matrix_order_dimensions($order, $unit = 'mm') {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_dimensions = array();
  // Loop over each line item on the order.
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {

    // Get the weight value of product line items.
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {

  $line_item_dimensions = commerce_shipping_matrix_product_line_item_dimensions($line_item_wrapper->value());

      $order_dimensions = array_merge($order_dimensions, $line_item_dimensions);
      }
    }

  // Now ensure that all dimensions supplied are in the requested units
  foreach ($order_dimensions as $key => $dimensions) {
    $order_dimensions[$key] = physical_dimensions_convert($dimensions, $unit);
  }
  return $order_dimensions;
  }
