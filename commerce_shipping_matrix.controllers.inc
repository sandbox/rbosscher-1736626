<?php

/**
 * @file
 * Enitity Controlers for the Shipping Matrix Module
 */


/**
 * ShippingMatrixShippingServiceControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * see hook_entity_info_alter()
 */

interface ShippingMatrixShippingServiceControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * EntityExampleBasicController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class ShippingMatrixShippingServiceController
  extends DrupalDefaultEntityController
  implements ShippingMatrixShippingServiceControllerInterface {

  /**
   * Create and return a new entity_example_basic entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'shipping_matrix_services';
    $entity->bundle_type = 'shipping_matrix_services';
    $entity->is_new = TRUE;
    //$entity->shipping_service_id = false;
    $entity->item_description = '';
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'shipping_matrix_services');

    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = array();
    if (isset($entity->shipping_service_id)) { 
      $primary_keys = $entity->shipping_service_id ? 'shipping_service_id' : array();
    }
  
    // Write out the entity record.
    // table to write to, entity data to write to table, primairy key
    drupal_write_record('shipping_matrix_shipping_service', $entity, $primary_keys);

    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
  

    if (empty($primary_keys)) {
      field_attach_insert('shipping_matrix_services', $entity);
    }
    else {
      field_attach_update('shipping_matrix_services', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'shipping_matrix_services');

    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more entity_example_basic entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $basic_ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $shipping_service_id = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('shipping_matrix_services_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'shipping_matrix_shippingservice');
          field_attach_delete('shipping_matrix_services', $entity);
          $shipping_service_id[] = $entity->shipping_service_id;
        }
        db_delete('shipping_matrix_shipping_service')
          ->condition('shipping_service_id', $shipping_service_id, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('shipping_matrix', $e);
        throw $e;
      }
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * ShippingMatrixZonesControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * see hook_entity_info_alter()
 */

interface ShippingMatrixZonesControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * EntityExampleBasicController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class ShippingMatrixZonesController
  extends DrupalDefaultEntityController
  implements ShippingMatrixZonesControllerInterface {

  /**
   * Create and return a new entity_example_basic entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'shipping_matrix_zone';
    $entity->shipping_zone_id = 0;
    $entity->bundle_type = 'shipping_matrix_zone';
    $entity->item_description = '';
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    if (empty($entity->shipping_zone_id)) {
      $entity->created = time();
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'shipping_matrix_zone');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->shipping_zone_id ? 'shipping_zone_id' : array();
    // Write out the entity record.
    drupal_write_record('shipping_matrix_shipping_zone', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('shipping_matrix_zone', $entity);
    }
    else {
      field_attach_update('shipping_matrix_zone', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'shipping_matrix_zone');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more entity_example_basic entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $basic_ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $shipping_zone_id = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('shipping_matrix_zone_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'shipping_matrix_zone');
          field_attach_delete('shipping_matrix_zone', $entity);
          $shipping_zone_id[] = $entity->shipping_zone_id;
        }
        db_delete('shipping_matrix_shipping_zone')
          ->condition('shipping_zone_id', $shipping_zone_id, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('shipping_matrix', $e);
        throw $e;
      }
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



/**
 * ShippingMatrixTableWeightControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * see hook_entity_info_alter()
 */

interface ShippingMatrixTableWeightControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * EntityExampleBasicController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class ShippingMatrixTableWeightController
  extends DrupalDefaultEntityController
  implements ShippingMatrixTableWeightControllerInterface {

  /**
   * Create and return a new entity_example_basic entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'shipping_matrix_weight_table';
    $entity->weight_table_id = 0;
    $entity->bundle_type = 'shipping_matrix_weight_table';
    $entity->item_description = '';
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'shipping_matrix_weight_table');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->weight_table_id ? 'weight_table_id' : array();
    // Write out the entity record.
    drupal_write_record('shipping_matrix_weight_table', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('shipping_matrix_weight_table', $entity);
    }
    else {
      field_attach_update('shipping_matrix_weight_table', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'shipping_matrix_weight_table');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more entity_example_basic entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $basic_ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $weight_table_id = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('shipping_matrix_weight_table_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'shipping_matrix_weight_table');
          field_attach_delete('shipping_matrix_weight_table', $entity);
          $weight_table_id[] = $entity->weight_table_id;
        }
        db_delete('shipping_matrix_weight_table')
          ->condition('weight_table_id', $weight_table_id, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('shipping_matrix', $e);
        throw $e;
      }
    }
  }
}


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


